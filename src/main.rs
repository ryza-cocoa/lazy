use clap::{Arg, App, SubCommand};
use hmac::{Hmac, Mac};
use reqwest::{Client, header};
use sha2::Sha256;
use std::time::{SystemTime, UNIX_EPOCH};

// API Access Token (replace the string below)
static APITOKEN: &str = "applesmc";
// API Access Secret (replace the string below)
static APISECRET: &str = "secret";

// alias for HMAC-SHA256
type HmacSha256 = Hmac<Sha256>;

/// Get UNIX timestamp in mill seconds
///
/// # Examples
///
/// ```
/// println!("{}", unix_timestamp_millis());
/// ```
fn unix_timestamp_millis() -> String {
    let start = SystemTime::now();
    let since_the_epoch = start.duration_since(UNIX_EPOCH).expect("Time went backwards");
    let millis_since_the_epoch = since_the_epoch.as_millis();
    format!("{}", millis_since_the_epoch)
}

/// Generate HMAC-SHA265 of given message, secret and salt
///
/// # Examples
///
/// ```
/// println!("{}", generate_hmac("message", "secret", "salt"));
/// ```
fn generate_hmac(message: &str, secret: &str, salt: &str) -> String {
    // Create HMAC-SHA256 instance which implements `Mac` trait
    let mut mac = HmacSha256::new_varkey(format!("{}{}", secret, salt).as_bytes())
        .expect("HMAC can take key of any size");
    mac.input(message.as_bytes());

    // `result` has type `MacResult` which is a thin wrapper around array of
    // bytes for providing constant time equality check
    let mut stringfied_hmac = String::new();
    for code in mac.result().code() {
        stringfied_hmac.push_str(&format!("{:x}", code));
    }
    
    stringfied_hmac
}

fn main() {
    let matches = App::new("breezin")
        .version("1.1")
        .author("Ryza <1-ryza@users.noreply.magic.ryza.moe>")
        .about("Remote control fan speed on a Mac which runs Linux")
        .arg(Arg::with_name("api")
            .short("a")
            .long("api")
            .value_name("API")
            .help("API server IP:Port")
            .required(true)
        )
        .subcommand(SubCommand::with_name("get")
                    .about("Get infomation")
                    .version("1.1")
                    .author("Ryza <1-ryza@users.noreply.magic.ryza.moe>")
                    .arg(Arg::with_name("type")
                        .short("t")
                        .long("type")
                        .value_name("TYPE")
                        .help("[fan|temp]")
                        .required(true)
                    )
                    .arg(Arg::with_name("name")
                        .short("n")
                        .long("name")
                        .value_name("NAME")
                        .help("name OR no name to get all")
                        .required(false)
                    )
        )
        .subcommand(SubCommand::with_name("set")
                    .about("Set value of property to specified fan")
                    .version("1.1")
                    .author("Ryza <1-ryza@users.noreply.magic.ryza.moe>")
                    .arg(Arg::with_name("name")
                        .short("n")
                        .long("name")
                        .value_name("NAME")
                        .help("name")
                        .required(true)
                    )
                    .arg(Arg::with_name("property")
                        .short("p")
                        .long("property")
                        .value_name("PROPERTY")
                        .help("property")
                        .required(true)
                    )
                    .arg(Arg::with_name("value")
                        .short("v")
                        .long("value")
                        .value_name("VALUE")
                        .help("set value to property")
                        .required(true)
                    )
        )
        .get_matches();

    // get UNIX timestamp
    let timestamp = unix_timestamp_millis();

    // generate access token
    let access_token = generate_hmac(APITOKEN, APISECRET, &timestamp);
    
    // customize headers
    let mut headers = header::HeaderMap::new();
    headers.insert(header::AUTHORIZATION, header::HeaderValue::from_str(&access_token).unwrap());
    headers.insert("TIMESTAMP", header::HeaderValue::from_str(&timestamp).unwrap());
    
    // build custom client with access token
    let client = Client::builder()
        .default_headers(headers)
        .build().unwrap();
        
    // get api server
    let api = matches.value_of("api").unwrap().to_string();

    // build api endpoint
    let mut api_endpoint: String = String::new();
    api_endpoint.push_str(&api);
    if api_endpoint.chars().last().unwrap() != '/' {
        api_endpoint.push_str("/");
    }
    api_endpoint.push_str("api/v1/");

    // check subcommand
    if let Some(matches) = matches.subcommand_matches("set") {
        // get name, property and value
        let name = matches.value_of("name").unwrap().to_string();
        let property = matches.value_of("property").unwrap().to_string();
        let value = matches.value_of("value").unwrap().to_string();
        
        // given that only fans can be controlled
        api_endpoint.push_str("fans/");
        api_endpoint.push_str(&name);
        
        // generate json body
        let mut json_body: serde_json::Value = serde_json::from_str("{}").unwrap();
        json_body["property"] = serde_json::Value::from(property);
        json_body["value"] = serde_json::Value::from(value);
        let json = serde_json::to_string(&json_body).unwrap();
        
        // send PUT request
        let response = client.put(&api_endpoint)
            .body(json)
            .send();
        
        // print response
        match response {
            Ok(mut res) => {
                match res.text() {
                    Ok(content) => {
                        println!("{}", content);
                    },
                    Err(e) => {
                        eprintln!("[ERROR] {:?}", e);
                    }
                }
            },
            Err(e) => {
                eprintln!("[ERROR] {:?}", e);
            }
        }
    } else if let Some(matches) = matches.subcommand_matches("get") {
        // fan or temp
        let type_get = matches.value_of("type").unwrap().to_string();
        if &type_get == "fan" {
            // we can get a single fan's status
            // if specified name
            if matches.is_present("name") {
                // get the specified name
                let name = matches.value_of("name").unwrap().to_string();
                
                // build api endpoint for specified fan
                api_endpoint.push_str("fans/");
                api_endpoint.push_str(&name);
            } else {
                // build api endpoint for all fans
                api_endpoint.push_str("fans");
            }
        } else {
            // build api endpoint for all temps
            api_endpoint.push_str("temps");
        }
        
        // send GET request
        let response = client.get(&api_endpoint).send();
        
        // print response
        match response {
            Ok(mut res) => {
                match res.text() {
                    Ok(content) => {
                        println!("{}", content);
                    },
                    Err(e) => {
                        eprintln!("[ERROR] {:?}", e);
                    }
                }
            },
            Err(e) => {
                eprintln!("[ERROR] {:?}", e);
            }
        }
    }
}
