# lazy
Rust Learning. RESTful API client of Breezin

### Details
You may refer to my blog post [here](https://ryza.moe/2019/10/rust-learning-from-zero-11/).

### Screenshot
![screenshot 1](breezin-restful-lazy-get-set.png)

![screenshot 2](breezin-restful-lazy-help.png)
